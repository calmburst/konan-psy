import './styles.css';
import React  from 'react';
import helper from '../../helper';

export default class Slider extends React.Component {
    renderSection() {
        const children = helper.convertToArray(this.props.children);
        var item = children.map(function(_item) {
            return _item
        });
        return item;
    }
    render() {
    	const {about, title} = this.props;
        return (
           <div className="info-block">
                    <div className="info-block-header">
                        <div className="info-block-title">{title}</div>
                        <div className="info-block-about">{about}</div>
                    </div>
                    <div>{this.renderSection()}</div>
                </div>
        )
    }
}