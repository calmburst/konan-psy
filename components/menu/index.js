import './styles.css';

import React       from 'react';
import { connect } from 'react-redux'

import {getMenu} from '../../actions/menu';

class Menu extends React.Component {
    componentWillMount() {
        this.props.dispatch(getMenu());
    }
    render() {
        const {menu=[]} = this.props;
        return (
            <div className="konan-menu">
                <a href="#/" className="konan-menu-logo" />
                <div className="konan-menu-right">
                    <ul>
                        {
                            menu && menu.length && menu.map((m,i) => {
                                return (
                                    <li key={i} className="konan-menu-item">
                                        {(m.children && m.children.length>0) ? 
                                            <a href={m.href}> {m.name} &#9662; </a>
                                            :
                                            <a href={m.href}> {m.name}</a>
                                        }
                                        {m.children && m.children.length>0 && 
                                            <ul className="dropdown">
                                                {
                                                    m.children.map((child,j)=>{
                                                        return ( 
                                                            <li key={j}><a href={child.href}>{child.name}</a></li>
                                                        )
                                                    })
                                                }
                                            </ul>
                                        }
                                    </li> 
                                )
                            })
                        }
                    </ul>
                </div> 
            </div> 
        )
    }
}
const mapStateToProps = (state) => {
    return {
        menu : state.menu,
    }
}
export default connect (mapStateToProps) (Menu);