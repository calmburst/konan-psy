
import React from 'react';
import classnames from 'classnames';

import './styles.css';

export default class Steps extends React.Component {
    render () {
        const steps = this.props.steps && this.props.steps.map((step, i) => {
            let className = classnames({
                ["steps__item"]: true,
                ["steps__item_active"]: step.active,
                ["steps__item_green"]: step.green 
            });
            return <span key={i} className={className}>
               {step.title}
            </span>;
        }, this);
        
        return (
            <div className="steps">
                {steps}
            </div>
        );
    }
}

Steps.propTypes = {
    steps: React.PropTypes.arrayOf(React.PropTypes.shape({
        title: React.PropTypes.string.isRequired,
        active: React.PropTypes.bool
    })).isRequired
};