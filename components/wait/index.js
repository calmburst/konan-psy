import React from 'react';

import {Component} from 'react';

import './styles.css';
import ring from './img/ring.svg';

export default class Wait extends Component {

    constructor(props) {
        super(props);       
    } 
    
    render() {
        let {
            size = 1,
            fixedWait,
            mini
        } = this.props,
        classname = fixedWait ? " wait2__o1_fixed" : "";
        classname = mini ? " wait2__o1_mini" : "";

        return (
            <div className={"wait2__o1" + classname}>
                <div className="wait2__o2">
                    {size<=0 && <img src={ring} width='20' height='20' />}
                    {size==1 && <img src={ring} width='50' height='50'/>}
                    {size==2 && <img src={ring} width='100' height='100'/>}
                    {size>=3 && <img src={ring} width='200' height='200'/>}
                </div>
            </div>
        )
    }
}
