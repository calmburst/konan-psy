import './styles.css';

import React  from 'react';

export default class Header extends React.Component {
    render() {
    	const {clickAsk} = this.props;
        return (
           <div className="konan-header">
                <div className="konan-header-right konan-header-email"> da8530@mail.ru</div>
                <div className="konan-header-right konan-header-phone"> +7 (964) 553-66-28</div>
                <div className="konan-header-right konan-header-link" onClick={clickAsk}>Задать вопрос</div>
            </div>
        )
    }
}