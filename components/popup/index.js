
import React from 'react';

import {Component} from 'react';

import Wait from '../wait';
import Steps from '../steps';
import helper from '../../helper';

import './styles.css';
//
const KEY_ESC = 27;


export default class Popup extends Component {
    constructor(props) {
        super(props);
        this.handlerKeyDown = this.handlerKeyDown.bind(this);
    }
    
    componentDidMount(){
        document.addEventListener('keydown', this.handlerKeyDown);
    }
    componentWillUnmount(){
        document.removeEventListener('keydown', this.handlerKeyDown);
    }
    
    handlerKeyDown(event){
        if (event.keyCode==KEY_ESC) {
            this.close();
        };
    } 

    close() {
        this.props.onClose();
    }

    checkChildren() {  
        const children = helper.convertToArray(this.props.children);

        for (var i in children) {
            if (children[i] && children[i].props && children[i].props['popup-header']) {
                return true;
            }
        };
        return false;
    }

    renderSteps() {
        let steps;
        const children = helper.convertToArray(this.props.children);

        for (var i in children) {
            if (children[i] && children[i].props && children[i].props['popup-header']) {
                steps = children[i].props['popup-header'].map((_item, i) => {
                    let {step} = this.props;
                    let className = "actions-popup__container__steps__item";
                    if (_item.step < step && !_item.active) {
                        className += " actions-popup__container__steps__item_green";
                    }
                    if (_item.active) {
                        className += " actions-popup__container__steps__item_active";
                    }
                    return (
                        <span className={className}>{_item.label}</span>
                    )
                }, this);
            }
        };
        return steps;
    }

    renderSection() {
        const children = helper.convertToArray(this.props.children);
        var item = children.map(function(_item) {
            return _item
        });
        return item;
    }

    render() {
        const {
            id = 'popup',
            title,
            classname = '',
            locked,
        } = this.props,
        close = this.close.bind(this);
        var hasChildren = this.checkChildren();
        return (
            <div id={id} className={'actions-popup ' + classname}>
                <div className="actions-popup__bg"></div>
                <div className="actions-popup__fr">
                    <div className="actions-popup__fr__cell">
                        <div className="actions-popup__wrap">
                            <div className="actions-popup__container">
                                {locked && <Wait />}
                                <div className="actions-popup__container__close-icon" onClick={close}></div>
                                <div className="actions-popup__container__header">{title}</div>
                                {hasChildren && <div className="actions-popup__container__steps">{this.renderSteps()}</div>}
                                <div className="actions-popup__container__content">{this.renderSection()}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

Popup.propTypes = {
    title: React.PropTypes.string,
    // width: any valid css width value: 600px, 100%, etc...
    width: React.PropTypes.string
};
