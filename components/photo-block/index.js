import './styles.css';

var Comments = require('react-icons/lib/fa/comments');
var Calendar = require('react-icons/lib/fa/calendar');
var Facebook = require('react-icons/lib/fa/facebook');
var Newspaper = require('react-icons/lib/fa/newspaper-o');
var Vk = require('react-icons/lib/fa/vk');
var YouTube = require('react-icons/lib/fa/youtube');
import React  from 'react';

export default class Slider extends React.Component {
    render() {
        return (
           <div className="konan-slider">
           		<div className="konan-slider-left">
           			<div className="konan-slider-title"> алексей данильченко </div>
           			<div className="konan-slider-text"> Институт Алексея Данильченко (ИАД) создан Алексеем Данильченко для осуществления работ по созданию и исследованию  методики Алексея Данильченко, ее использованию и применению в карательных целях на жертвах бытового и производственного анального насилия </div>
           			<ul className="konan-slider-list">
           				<li>Работает даже под водой и в космосе</li>
           				<li>Позволяет запускать спутники на орбиту</li>
           				<li>Увеличивает зарплату и МПХ в 10 раз </li>
           			</ul>
           			<ul className="konan-slider-list-icons">
	           			<li className="icon"> {React.createElement(Comments, null)}</li>
	           			<li className="icon"> {React.createElement(Newspaper, null)}</li>
	           			<li className="icon"> {React.createElement(Calendar, null)}</li>
	           			<li className="icon"> {React.createElement(Facebook, null)}</li>
	           			<li className="icon"> {React.createElement(YouTube, null)}</li>
	           			<li className="icon"> {React.createElement(Vk, null)}</li>
	           			<li className="button"> ПОДРОБНЕЕ </li>
	           		</ul>
           		</div>
            </div>
        )
    }
}