
// import './theme/default/theme';
import { createHashHistory }                from 'history';
import { Provider }                         from 'react-redux';
import React                                from 'react';
import {render}                             from 'react-dom';
import ReactRedux, { connect }              from 'react-redux';
import Redux, { bindActionCreators }        from 'redux';
import store                                from './store';

import {
    useRouterHistory,
    RouterContext,
    IndexRoute,
    Link,
    Redirect,
    Route,
    Router
}                                           from 'react-router';

import PageMain                             from './page/main';
import PageArticles                         from './page/articles';


class App extends React.Component {

    constructor(props, context) {
        super(props, context);
    }
    
    render() {
        return (
            <div id="App">
                {
                    React.cloneElement(this.props.children, {
                        s: {},
                        l: {}
                    })
                }

            </div>
        );
    }
}
///


const appHistory = useRouterHistory(createHashHistory)({
    queryKey: false
});

render(
    (
        <Provider store={store}>
            <div>
                <Router history={appHistory}>
                    <Route path="/" component={App}>
                        <IndexRoute component={PageMain} />
                        <Route path="articles" component={PageArticles}/>
                        <Route path="*" component={PageMain}/>
                    </Route>
                </Router>
            </div>
        </Provider>
    )
    , document.getElementById('body')
);
