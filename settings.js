
var settings = {
    host: '', //хост, куда отправятся ajax-запросы
    host_sds:'',
    theme : 'theme__default',

    //todo: [] rude hack, set 2 again
    ajaxTimeout: 20, //максимальное время ожидания ответа от сервера на ajax-запрос//
    dateFormat: 'DD.MM.YYYY',
    dateLongFormat : 'Do MMM YYYY',
    dateTimeFormat : 'DD MMMM, HH:mm',
    dateLongFormatDivided: 'Do MMMM, YYYY',

    bankBranchesPage:'http://www.google.ru',

    loginErrorHideTime: 20,

    sessionExpTime : 30,
    keepAliveTiming : 10,

    smsOtpLength: 5,
    mTokenOtpLength: 5,

    otpErrorHideTime: 20,
    otpSmsPauseBeforeMsg1: 7,
    otpSmsPauseBeforeMsg2: 7,
    otpHelpCount: 666,

    otpMTokenOnlineTimeout: 30,
    otpMTokenOnlineRetryTime: 5,

    minPasswordLength: 8,
    msxPasswordLength: 16,
    minLowerLetterQuantity: 1,
    minCapitalLetterQuantity: 1,
    signupMinNumberQuantity: 1,
    maxIdenticalCharactersInSuccession: 3,
    forbiddenPatterns: [],
    signupAllowedPattern: '^[a-zA-Zа-яА-Я0-9ÄÖẞÜÁČĎÉĚÍŇÓŘŠŤÚŮÝŽČĆǅĐǈǋŠŽäößüáčďéěíňóřšťúůýžčćǆđǉǌšž\«\»\!\"\#\$\%\&\'\(\)\*\+\,\-\.\/\:\;\<\=\>\?\@\[\\\]\^\_\.\`\{\|\}\~]*$',

    MaximumExportedTransactionsPerFile: 100,

    signupErrorHideTime: 20,
    signupSmsPauseBeforeMsg1: 7,
    signupSmsPauseBeforeMsg2: 7,
    signupSmsHelpCount: 3,

    formSmsPauseBeforeMsg1: 5,
    otpRepeatTime: 30,

    turnJsOpera: 'http://www.enable-javascript.com/#opera',
    turnJsChrome: 'http://www.enable-javascript.com/#chrome',
    turnJsFirefox: 'http://www.enable-javascript.com/#firefox',
    turnJsIE: 'http://www.enable-javascript.com/#ie',
    turnJsSafati: 'http://www.enable-javascript.com/#safari',
    updateChrome: 'https://www.google.com/intl/cs/chrome/browser/desktop/',
    updateFirefox: 'https://www.mozilla.org/cs/firefox/new/',
    updateIE: 'http://windows.microsoft.com/cs-cz/internet-explorer/ie-11-worldwide-languages',
    updateOpera: 'http://www.opera.com/cs',
    updateSafari: 'https://www.apple.com/support/mac-apps/safari',

    warningBeforeDebitCardExpiration : 70,
    warningBeforeCreditCardExpiration : 70,
    daysOfAvailabilityToCancelDebitCardAutoReissue : 42,
    daysOfAvailabilityToCancelCreditCardAutoReissue : 42,
    daysOfAvailabilityToChangeDebitCardDeliveryMethod : 42,
    daysOfAvailabilityToChangeCreditCardDeliveryMethod : 42,
    DebitCardStatusDeactivatedNumber: 10,
    CreditCardStatusDeactivatedNumber: 10,
    CreditCardStatusPermBlockVis: 1,
    CreditCardStatusPermBlockNumber: 10,
    depositMaturityWarningDays : 30,
    overdraftIsPastWarningDays : 7,
    overdraftOverdueApproachingDays : 7,
    maxExecutionDate: 365,

    minLengthAccountPrefix: 2,
    minLengthAccountNumber: 2,

    CounterPartiesLimit: 5,

    futureDateDomesticTransferMaxDays: 365,
    futureDateForeignTransferMaxDays: 90,
    futureDateOwnAccTransferMaxDays: 90,

    culture: {
        numberFormat: {
            decimals: 2,
            ',': ' ',
            '.': ',',
            groupSize: [3],
            currency: {
                decimals: 2,
                ',': ' ',
                '.': ',',
                groupSize: [3],
                maskMoney: '0 000 000 000 000'
            }
        },
        rules: {
            // just a digit
            '0': /\d/,
            // as in String
            'C': /[a-zA-Z]/,
            // leading zero
            '#': '0'
        }
    },

    tempBlockingPeriod: '60',
    smsWrongLoginBeforeTempBlock: '3',
    smsWrongLoginBeforePermBlock: '6',

    keys: {
        BACKSPACE: 8,
        COMMA: 188,
        DELETE: 46,
        DOWN: 40,
        END: 35,
        ENTER: 13,
        ESCAPE: 27,
        HOME: 36,
        LEFT: 37,
        PAGE_DOWN: 34,
        PAGE_UP: 33,
        PERIOD: 190,
        RIGHT: 39,
        SPACE: 32,
        TAB: 9,
        UP: 38
    },
    errorCodes: {
        "-1":   "ERRCODE_1000_UNKNOWN_ERROR",
        0:      "ERRCODE_0_SUCCESS",
        1:      "ERRCODE_1_ERROR",
        666:    "ERRCODE_666_OTP_EXPIRED",
        888:    "ERRCODE_888_WRONG_CONFIRM_CODE",
        999:    "ERRCODE_999_PASSWORD_INCORRECT",
        1000:   "ERRCODE_1000_UNKNOWN_ERROR",
        1001:   "ERRCODE_1001_WRONG_PIN_ONCE",
        1002:   "ERRCODE_1002_WRONG_PIN_WAIT",
        1003:   "ERRCODE_1003_WRONG_PIN_BLOCK",
        1004:   "ERRCODE_1004_MGUID_INCORRECT",
        1005:   "ERRCODE_1005_USER_TEMP_BLOCKED",
        1006:   "ERRCODE_1006_SESSION_INVALID",
        1007:   "ERRCODE_1007_SESSION_TIMEOUT",
        1008:   "ERRCODE_1008_DEVICE_BLOCKED",
        1009:   "ERRCODE_1009_REG_LIMIT_REACHED",
        1010:   "ERRCODE_1010_REG_CODE_ERROR",
        1011:   "ERRCODE_1011_REG_CODE_EXPIRED",
        1012:   "ERRCODE_1012_DEVICE_ALREADY_ACTIVE",
        1013:   "ERRCODE_1013_DEVICE_LIMIT_REACHED",
        1014:   "ERRCODE_1014_LOGIN_ID_NOT_EXISTS",
        1015:   "ERRCODE_1015_MOB_CHANNEL_BLOCKED",
        1016:   "ERRCODE_1016_AUTHORIZATION_REQUIRED",
        1017:   "ERRCODE_1017_LIMIT_INCORRECT",
        1018:   "ERRCODE_1018_BACKEND_UNAVAILABLE",
        1019:   "ERRCODE_1019_USER_PERM_BLOCKED",
        1020:   "ERRCODE_1020_MOBILE_CHANNEL_NOT_ACTIVE",
        1021:   "ERRCODE_1021_DEVICE_INACTIVE",
        1022:   "ERRCODE_1022_NO_PHONE_NUMBER",
        1023:   "ERRCODE_1023_UNABLE_TO_COMPLETE_OPERATION",
        1024:   "ERRCODE_1024_PRODUCT_ALIAS_DUPLICATE",
        1025:   "ERRCODE_1025_PRODUCT_UNAVAILABLE",
        1026:   "ERRCODE_1026_TRANSFER_DETAILS_INVALID",
        1027:   "ERRCODE_1027_CHANNEL_LIMIT_EXCEEDED",
        1028:   "ERRCODE_1028_INTERNET_CHANNEL_NOT_ACTIVE",
        1033:   "ERRCODE_1033_NO_PERS_MANAGER",
        1034:   "ERRCODE_1034_CORECLIENTID_NOT_FOUND",
        1035:   "ERRCODE_1035_AFRAUD_DENY_USER",
        1036:   "ERRCODE_1036_AFRAUD_DENY_OPERATION",
        1037:   "ERRCODE_1037_AFRAUD_REVIEW",
        1038:   "ERRCODE_1038_TIMEOUT_ERROR",
        1039:   "ERRCODE_1039_TMPL_NOT_FOUND",
        1040:   "ERRCODE_1040_PRODUCT_NOT_FOUND",
        1041:   "ERRCODE_1041_MC_NOT_FOUND",
        1042:   "ERRCODE_1042_UNABLE_TO_OPEN_NEW_PRODUCT",
        1043:   "ERRCODE_1043_SO_NOT_FOUND",
        1044:   "ERRCODE_1044_PO_NOT_FOUND",
        1045:   "ERRCODE_1045_PO_ALREADY_PROCESSED",
        1046:   "ERRCODE_1046_PO_ALREADY_CANCELLED",
        1047:   "ERRCODE_1047_PO_CUT_OF_TIME",
        1048:   "ERRCODE_1048_PO_OPERATION_NOT_AVAILABLE",
        1049:   "ERRCODE_1049_SO_ALREADY_CANCELLED",
        1050:   "ERRCODE_1050_MC_ALREADY_CANCELLED",
        1051:   "ERRCODE_1051_SENDING_ERROR",
        1052:   "ERRCODE_1052_ID_NOT_VALID",
        1053:   "ERRCODE_1025_PRODUCT_UNAVAILABLE",
        1054:   "ERRCODE_1054_REG_CODE_RESET",
        1056:   "ERRCODE_1056_NOT_ENOUGH_MONEY",
        1057:   "ERRCODE_1057_SIMULTANEOUS_ACCESS",
        1058:   "ERRCODE_1058_DELETE_CUT_OFF",
        1059:   "ERRCODE_1059_SESSION_MISMATCH",
        1061:   "ERRCODE_1061_PRODUCT_FOR_PO_NOT_FOUND",
        1062:   "ERRCODE_1062_TMPL_NAME_ALREADY_USED",
        1063:   "ERRCODE_1063_MA_VERSION_OUTDATED",
        1064:   "ERRCODE_1064_SERVICE_UNAVAILABLE",
        1065:   "ERRCODE_1065_REGISTRATION_RESTRICTED",
        1111:   "ERRCODE_1111_UNSUPPORTED_BROWSER",
        2000:   "ERRCODE_2000_LOGIN_PWD_NOT_EXIST",
        2001:   "ERRCODE_2001_CLIENT_BLOCKED",
        2003:   "ERRCODE_2003_PASSWORD_EXPIRED",
        2004:   "ERRCODE_2004_LOGIN_NOT_FOUND",
        2005:   "ERRCODE_2005_INCORRECT_PASSWORD_MINPASSWORDLENGTH",
        2006:   "ERRCODE_2006_INCORRECT_PASSWORD_MAXPASSWORDLENGTH",
        2007:   "ERRCODE_2007_INCORRECT_PASSWORD_MAXIDENTICALCHARACTERSINSUCCESSION",
        2008:   "ERRCODE_2008_INCORRECT_PASSWORD_MINCAPITALLETTERQUANTITY",
        2009:   "ERRCODE_2009_INCORRECT_PASSWORD_MINLOWERLETTERQUANTITY",
        2010:   "ERRCODE_2010_INCORRECT_PASSWORD_MINDIGITQUANTITY",
        2011:   "ERRCODE_2011_INCORRECT_PASSWORD_ADDITIONALSYMBOLS",
        2012:   "ERRCODE_2012_INCORRECT_PASSWORD_FORBIDDENPATTERNS",
        2013:   "ERRCODE_2013_INCORRECT_PASSWORD_UNSUPPORTED_SYMBOLS",
        2020:   "ERRCODE_2020_O_APPROVAL_REASON_NEEDED",
        2021:   "ERRCODE_2021_DUPLICATE_SUPPORTED_VERSION",
        2030:   "ERRCODE_2030_INVALID_DICTIONARY",
        2031:   "ERRCODE_2031_DICTIONARY_UPLOAD_TIMEDOUT",
        2032:   "ERRCODE_2032_DICTIONARY_CONCURENT_UPLOAD",
        2040:   "ERRCODE_2040_INVALID_USER_LOGIN",
        2041:   "ERRCODE_2041_INVALID_USER_STATUS",
        2042:   "ERRCODE_2042_PO_INVALID_STATUS",
        2045:   "ERRCODE_2045_PO_PROCESSING_ALREADY_IN_PROGRESS",
        2046:   "ERRCODE_2046_PO_PROCESSING_ALREADY_FINISHED",
        2047:   "ERRCODE_2047_PO_PROCESSING_DOESNT_EXIST",
        2050:   "ERRCODE_2050_CONFIG_PARAMETER_NOT_FOUND",
        2051:   "ERRCODE_2051_ALERT_NOT_FOUND",
        2052:   "ERRCODE_2052_CONFIG_PARAMETER_INVALID_HANDLER",
        2060:   "ERRCODE_2060_LOGIN_IS_DISABLED",
        2071:   "ERRCODE_2071_MESSAGE_NOT_FOUND",
        2667:   "ERRCODE_2667_SERVICE_ID_INCORRECT",
        2043:   "ERRCODE_2043_CONFIG_PARAMETER_HANDLER_ERROR",
        2044:   "ERRCODE_2044_CONFIG_PARAMETER_VALIDATION_ERROR",
        2048:   "ERRCODE_2048_PO_ALREADY_ACCEPTED_BY_AF",
        2049:   "ERRCODE_2049_PO_ALREADY_REJECTED_BY_AF",
        2061:   "ERRCODE_2061_FAILED_TO_SEND_MESSAGE",
        2090:   "ERRCODE_2090_PASSWORDS_MISMATCH_EMPTY",
        2091:   "ERRCODE_2091_PASSWORDS_MISMATCH",
        2092:   "ERRCODE_2092_NEW_PASSWORD_INCORRECT",
        3001:   "ERRCODE_3001_PHONE_NOT_ASSOCIATED",
        3002:   "ERRCODE_3002_SMS_LIMIT_EXCEEDED",
        3003:   "ERRCODE_3003_IMSI_CHECK_FAILED",
        3004:   "ERRCODE_3004_CODE_EXPIRED",
        3005:   "ERRCODE_3005_CODE_INCORRECT",
        3006:   "ERRCODE_3006_REGISTRATION_NEED_RESTART",
        4001:   "ERRCODE_4001_METHOD_UNREGISTERED",
        4002:   "ERRCODE_4002_METHOD_INACTIVE",
        4003:   "ERRCODE_4003_METHOD_TEMP_BLOCKED",
        4004:   "ERRCODE_4004_METHOD_BLOCKED",
        7000:   "ERRCODE_7000_CAPTCHA_ERROR"
    },
    cmsUrl:'http://sberbank.eu/need-help',
    secPhoneNumber:  'http://sberbank.eu/need-help'
};

for (var i in settings) if (typeof settings[i] === 'function') settings[i] = settings[i].bind(settings);

//export default settings;
module.exports = settings;
