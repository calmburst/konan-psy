var moment = require('moment');
var bigInteger = require('big-integer');
var settings = require('./settings');

var product_type_urls = {
    "LOAN_ACC": 'loan',
    "SAVINGS_ACC": 'saving-account',
    "CREDIT_CARD": 'credit-card',
    "CREDIT_ACC": 'credit-account',
    "CURRENT_ACC": 'current-account',
    "DEBIT_CARD": 'debit-card',
    "TERM_DEPOSIT": 'term-deposit',
    "COMBI": 'combi'
}

var IBANCharsMap = {
    A: "10",
    G: "16",
    M: "22",
    S: "28",
    Y: "34",
    B: "11",
    H: "17",
    N: "23",
    T: "29",
    Z: "35",
    C: "12",
    I: "18",
    O: "24",
    U: "30",
    D: "13",
    J: "19",
    P: "25",
    V: "31",
    E: "14",
    K: "20",
    Q: "26",
    W: "32",
    F: "15",
    L: "21",
    R: "27",
    X: "33"
};

var IBANCountryCodeList = ["AD", "AE", "AL", "AT", "AZ", "BA", "BE", "BG", "BH", "BR", "CH", "CR", "CY", "CZ", "DE", "DK", "DO", "EE", "ES", "FI", "FO", "FR", "GB", "GE", "GI", "GL", "GR", "GT", "HR", "HU", "IE", "IL", "IS", "IT", "JO", "KW", "KZ", "LB", "LI", "LT", "LU", "LV", "MC", "MD", "ME", "MK", "MR", "MT", "MU", "NL", "NO", "PK", "PL", "PS", "PT", "QA", "RO", "RS", "SA", "SE", "SI", "SK", "SM", "TL", "TN", "TR", "UA", "VG", "XK"];

//from http://stackoverflow.com/questions/7837456/how-to-compare-arrays-in-javascript
if (Array.prototype.equals) {
    // Warn if overriding existing method
    console.warn("Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code.");
}
// attach the .equals method to Array's prototype to call it on any array
Array.prototype.equals = function(array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time 
    if (this.length != array.length)
        return false;

    for (var i = 0, l = this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;
        } else if (this[i] != array[i]) {
            return false; // Warning - two different object instances will never be equal: {x:20} != {x:20}
        }
    }
    return true;
}
Object.defineProperty(Array.prototype, "equals", {
    enumerable: false
}); // Hide method from for-in loops

if (Array.prototype.find) {
    Object.defineProperty(Array.prototype, "find", {
        enumerable: false,
        value: function (predicate) {
            if (this == null) {
                throw new TypeError('Array.prototype.find called on null or undefined');
            }

            if (typeof predicate !== 'function') {
                throw new TypeError('predicate must be a function');
            }

            var list = Object(this);
            var length = list.length >>> 0;
            var context = arguments[1];

            for (var i = 0; i < length; i++) {
                var value = list[i];

                if (predicate.call(context, value, i, list)) {
                    return value;
                }
            }
        }
    });
}

//var helper = (function() {
//    return {
var helper = {
        isBottom: function(offset, cb) {
            // All thouse for IE back compatibility to 6 included.
            var me = this;
            var supportPageOffset = window.pageXOffset !== undefined;
            var isCSS1Compat = ((document.compatMode || '') === 'CSS1Compat');

            var y = supportPageOffset ? window.pageYOffset : isCSS1Compat ? document.documentElement.scrollTop : document.body.scrollTop;
            var innerHeight = window.innerHeight || document.documentElement.clientHeight;

            if((y + innerHeight) >= (me.getDocHeight() - offset)) {
                cb();
            }
        },

        getDocHeight: function() {
            var D = document;

            return Math.max(
                D.body.scrollHeight, D.documentElement.scrollHeight,
                D.body.offsetHeight, D.documentElement.offsetHeight,
                D.body.clientHeight, D.documentElement.clientHeight
            );
        },

        downloadFile: function(sUrl) {
     
            //If in Chrome or Safari - download via virtual link click
            if (this.downloadFile.isChrome || this.downloadFile.isSafari) {
                //Creating new link node.
                var link = document.createElement('a');
                link.href = sUrl;
         
                if (link.download !== undefined){
                    //Set HTML5 download attribute. This will prevent file from opening if supported.
                    var fileName = sUrl.substring(sUrl.lastIndexOf('/') + 1, sUrl.length);
                    link.download = fileName;
                }
         
                //Dispatching click event.
                if (document.createEvent) {
                    var e = document.createEvent('MouseEvents');
                    e.initEvent('click' ,true ,true);
                    link.dispatchEvent(e);
                    return true;
                }
            }
         
            // Force file download (whether supported by server).
            var query = '?download';
         
            window.open(sUrl + query, '_self');
        },

        bank2locales:function(bank) {
            switch (bank) {
                case 'SBAT':
                    return ['at', 'en', 'ru'];
                case 'SBCZ':
                    return ['cz', 'en', 'ru'];
                case 'SBDE':
                    return ['de', 'en', 'ru'];
                case 'SBHR':
                    return ['hr', 'en', 'ru'];
                default: //case 'SBRF':
                    return ['ru', 'en'];
            }
        },

        bank2currency: function(bank) {
            switch (bank) {
                case 'SBAT':
                    return 'EUR';
                case 'SBCZ':
                    return 'CZK';
                case 'SBDE':
                    return 'EUR';
                case 'SBHR':
                    return 'HRK';
                default: //case 'SBRF':
                    return 'RUB';
            }
        },

        bank2countryCode: function(bank) {
            switch (bank) {
                case 'SBAT':
                    return 'AT';
                case 'SBCZ':
                    return 'CZ';
                case 'SBDE':
                    return 'DE';
                case 'SBHR':
                    return 'HR';
                default: //case 'SBRF':
                    return 'RU';
            }
        },

        isValidInputText: function isValidInputText( str, lang ) {
        /*
        //MCPI-1088 надо разрешать везде любые символы, а то получается, что в английской локали нельзя руссики буквами писать. ужс же
            var regExp;

            if (lang === 'GE') {
                regExp = /[^ äößüÄÖẞÜ0-9a-zA-Z\«\»\!\"\#\%\$\&\\\'\(\)\*\+\,\-\.\/\:\;\<\>\=\?\@\[\]\^\_\`\{\}\|\~]+/g
            } else if (lang === 'CZ') {
                regExp = /[^ áčďéěíňóřšťúůýžÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ0-9a-zA-Z\«\»\!\"\#\%\$\&\\\'\(\)\*\+\,\-\.\/\:\;\<\>\=\?\@\[\]\^\_\`\{\}\|\~]+/g;
            } else if (lang === 'HR') {
                regExp = /[^ čćǆđǉǌšžČĆǅĐǈǋŠŽ0-9a-zA-Z\«\»\!\"\#\%\$\&\\\'\(\)\*\+\,\-\.\/\:\;\<\>\=\?\@\[\]\^\_\`\{\}\|\~]+/g;
            } else if (lang === 'RU') {
                regExp = /[^ а-яА-Я0-9a-zA-Z\«\»\!\"\#\%\$\&\\\'\(\)\*\+\,\-\.\/\:\;\<\>\=\?\@\[\]\^\_\`\{\}\|\~]+/g;
            } else {
                regExp = /[^ 0-9a-zA-Z\«\»\!\"\#\%\$\&\\\'\(\)\*\+\,\-\.\/\:\;\<\>\=\?\@\[\]\^\_\`\{\}\|\~]+/g
            }
        */
        var regExp = /[^ а-яА-ЯčćǆđǉǌšžČĆǅĐǈǋŠŽáčďéěíňóřšťúůýžÁČĎÉĚÍŇÓŘŠŤÚŮÝŽäößüÄÖẞÜ0-9a-zA-Z\«\»\!\"\#\%\$\&\\\'\(\)\*\+\,\-\.\/\:\;\<\>\=\?\@\[\]\^\_\`\{\}\|\~]+/g


            var results = [],
                result;

            while( (result = regExp.exec(str)) !== null ) {
                results.push(result);
                if (result.index === regExp.lastIndex) {
                    regExp.lastIndex++;
                }
            }

            return results.length === 0;
        },

        getLocalStorage: function() {
            return window.localStorage || {
                getItem: function() {},
                setItem: function() {},
                removeItem: function() {}
            }
        },

        // Is a given value a boolean?
        isBoolean: function(obj) {
            return obj === true || obj === false || Object.prototype.toString.call(obj) === '[object Boolean]';
        },

        checkSetting: function (setting) {
            if (this.isUndefined(setting)) {
                return true;
            }
            return setting !== '0';
        },

        checkDateBeforeAndLessThanSettings: function( setting, date ) {
            if (setting === undefined || date === undefined || setting === '-1') {
                return true;
            }

            date = moment(date).startOf('day');

            if (!date.isValid()) {
                return true;
            }

            var diff = date.diff(moment().startOf('day'), 'd');

    //         console.log('!DEBUG! helper checkDateBeforeAndLessThanSettings setting ',setting );
    //         console.log('!DEBUG! helper checkDateBeforeAndLessThanSettings date ',date );
    //         console.log('!DEBUG! helper checkDateBeforeAndLessThanSettings diff ', diff );
    //         console.log('!DEBUG! helper checkDateBeforeAndLessThanSettings result ', (diff > 0 && diff <= setting) );

            return diff > 0 && diff <= setting;
        },

        checkDatePastAndLessThanSettings: function( setting, date ) {
            // console.log('!DEBUG! helper checkDatePastAndLessThanSettings setting ',setting );
            // console.log('!DEBUG! helper checkDatePastAndLessThanSettings date ',date );
           
            if (setting === undefined || date === undefined || setting === '-1') {
                return true;
            }

            date = moment(date).startOf('day');

            if (!date.isValid()) {
                return true;
            }

            var diff = date.diff(moment().startOf('day'), 'd');
            
            // console.log('!DEBUG! helper checkDatePastAndLessThanSettings diff ', diff );
            // console.log('!DEBUG! helper checkDatePastAndLessThanSettings result ', (diff > 0 && diff <= setting) );
            
            return diff > 0 && diff <= setting;
        },

        checkDateBeforeAndBiggerThanSettings: function( setting, date ) {
            if (setting === undefined || date === undefined || setting === '-1') {
                return true;
            }

            date = moment(date);

            if (!date.isValid()) {
                return true;
            }

            var diff = date.diff(Date.now(), 'd');

            // console.log('!DEBUG! helper checkDateBeforeAndBiggerThanSettings setting ',setting );
            // console.log('!DEBUG! helper checkDateBeforeAndBiggerThanSettings date ',date );
            // console.log('!DEBUG! helper checkDateBeforeAndBiggerThanSettings diff ', diff );
            // console.log('!DEBUG! helper checkDateBeforeAndBiggerThanSettings result ', (diff > 0 && diff >= setting) );

            return diff > 0 && diff >= setting;
        },

        getUrlByProductType: function(productType, productId) {
            return '#/' + product_type_urls[productType] + '/' + productId;
        },

        endsWith: function(str, suffix) {
            return str.indexOf(suffix, str.length - suffix.length) !== -1;
        },

        //переводит number в строки - рекурсивно, по всему объекту
        'stringify' : function(o) {
            for (var i in o) {
                if (typeof o[i] == 'number') {
                    o[i] = o[i] + "";
                } else if (typeof o[i] == 'object') {
                    o[i] = helper.stringify(o[i]);
                }
            }
            return o;
        },

        //копирует объект - рекурсивно
        deep_copy : function(o, copyFunctions){
            if (o instanceof Array) {
                var ret = [];
                for (var i=0; i<o.length; i++) ret[i] = helper.deep_copy(o[i], copyFunctions);
                return ret;
            } else if (typeof o === 'object' && !(o instanceof RegExp)) {
                var ret = {};
                for (var i in o) if (copyFunctions || typeof o[i] !== 'function') ret[i] = helper.deep_copy(o[i], copyFunctions);
                return ret;
            } else {
                return o;
            }
        },

        //удаляет поля с пустыми строками
        deep_clear : function(o){
            var exclude = ['ip', 'subsidiary', 'devicePrint', 'domDataCollection', 'jsEvents', 'httpAccept', 'httpAcceptCharset', 'httpAcceptEncoding', 'httpAcceptLanguage', 'httpReferrer', 'userAgent', 'pageId'];
            if (typeof o === 'object' && !(o instanceof Array)) {
            for (var i in o) {
                    if (!~exclude.indexOf(i) && o[i] === '') delete o[i]; else helper.deep_clear(o[i]);
                }
            }
        },


        //форматирует число с разделением тысяч и добавлением ,00 (например, numberFormat(1000) == '1 000,00')
        numberFormat : function(n) {
            if (typeof n === 'undefined') {
                return '';
            } else if (typeof n === 'number' || !isNaN(n))  {
                //spaces
                var s = Math.floor(Math.abs(n)) + "";
                var l = s.length;
                var ret = "";
                for (var i=l; i>=0; i=i-3) {
                    ret = (i-3>0 ? " ":"") + s.substring(Math.max(0, i-3), i) + ret;
                }

                // TODO: А почему бы не заиспользовать .toFixed(2)?
                //2-digit after point
                var frac = Math.round(Math.abs(n%1*100));
                return (n<0?"-":"")  + ret + "," + (frac>9 ? "" : "0") + frac;
            } else {
                return n;
            }
        },
        numberFormatNoDot : function(n) {
            if (typeof n === 'undefined') {
                return '';
            } else if (typeof n === 'number' || !isNaN(n))  {
                //spaces
                var s = Math.floor(Math.abs(n)) + "";
                var l = s.length;
                var ret = "";
                for (var i=l; i>=0; i=i-3) {
                    ret = (i-3>0 ? " ":"") + s.substring(Math.max(0, i-3), i) + ret;
                }

                return (n<0?"-":"")  + ret;
            } else {
                return n;
            }
        },


        //форматирует номер карты - только первые 4 и последние 4, а в середине - точки + разделение каждых 4 цифр пробелами
        cardNumberFormat : function(number) {
            number = number.replace(/ /g, '');
            var s4 = number.substr(0, 4);
            var f4 = number.substr(-4);
        //·
        //•
            return (s4 + (number.length>8?Array(number.length-7).join('·'):'') + f4).replace(/(.{4})/g, '$1 ');
        },


        //форматирует дату (обычно DD.MM.YYYY)
        dateFormat : function(date) { // date is JS Date
            if (date === undefined) return undefined;
            var d = moment(date);
            return (d.isValid() ? d.format(settings.dateFormat) : date);
        },
        dateFormatDDMM : function(date) { // date is JS Date
            var d = moment(date);
            return (d.isValid() ? d.format('DD MMMM') : date);
        },

        // DD.MM.YYYY
        dateFormatDot : function(date) { // date is JS Date
            var d = moment(date);
            return (d.isValid() ? d.format('DD.MM.YYYY') : date);
        },

        getDateFormat : function(){
        return settings.dateFormat;
        },

        //форматирует дату (обычно Do MMM YYYY)
        dateLongFormat : function(date) { // date is JS Date
            var d = moment(date);
            return (d.isValid() ? d.format(settings.dateLongFormat) : date);
        },

        //форматирует дату (обычно Do MMMM, YYYY)
        dateLongFormatDivided : function(date) { // date is JS Date
            var d = moment(date);
            return (d.isValid() ? d.format(settings.dateLongFormatDivided) : date);
        },


        //форматирует дату и время (обычно DD MMMM, HH:mm)
        dateTimeFormat : function(date) { // date is JS Date
            var d = moment(date);
            return (d.isValid() ? d.format(settings.dateTimeFormat) : date);
        },

        // Return a copy of the object only containing the whitelisted properties.
        // (пришлось переписать на ES5, так как используется в mock-server.js)
        pick : function(o/*, ..fields*/) {
            "use strict";
            if (!o) o={}
            var result = {};

            for (var i=1; i<arguments.length; i++) {
                if (o.hasOwnProperty(arguments[i])) {
                    result[arguments[i]] = o[arguments[i]];
                }
            }
            return result;
        },

        // Return a copy of the object, properties named at arrExceptions deleted
        pickExcept : function(o, arrExceptions) {
                if (!o) return;
            var ret = this.deep_copy(o);
                for (var i in arrExceptions) delete ret[arrExceptions[i]];
                return ret;
        },


        //случайный id из букыв и цифер (длинной 5 символов)
        randomId : function(){
            return Math.random().toString(36).substr(2, 6);
        },

        //случайный номер из qty ненулевых цмфер
        randomNumber : function (qty){
            var ret=0;
            for (var i=0; i<qty; i++) ret = ret*10 + (Math.random()*8 + 1);
            return ret;
        },

        // Checks is a given variable an object.
        isObject : function(obj) {
            var type = typeof obj;

            return type === 'function' || type === 'object' && !!obj;
        },

        // Converts a given variable to array.
        convertToArray : function(objOrArr) {
            if (!this.isObject(objOrArr)) {
                return [];
            }
            return Array.isArray(objOrArr) ? objOrArr.slice(0) : [objOrArr];
        },

        // Checks is undefined a given variable.
        isUndefined : function(obj) {
            return obj === undefined;
        },

        // Retrieve the values of an object's properties.
        values : function(obj) {
            if (!this.isObject(obj)) return [];
            var keys = Object.keys(obj);
            var length = keys.length;
            var values = Array(length);
            for (var i = 0; i < length; i++) {
              values[i] = obj[keys[i]];
            }
            return values;
        },

        // Trim out all falsy values from an array.
        compact : function(arr) {
            if (!Array.isArray(arr)) {
                return [];
            }
            return arr.filter(function(i) {
              if (i) {
                  return true;
              }
              return false;
          });
        },

        noop : function () {},

        isUndefinedOrNull : function(obj) {
            return this.isUndefined(obj) || obj == null;
        },

        strRepeat : function(str, qty){
            if (qty < 1) return '';
            var result = '';
            while (qty > 0) {
              if (qty & 1) result += str;
              qty >>= 1, str += str;
            }
            return result;
        },

        // Checks is empty given string.
        isBlank: function(str) {
            if (str == null) str = '';
            return (/^\s*$/).test(str);
        },

        // Checks whether the currency is National or not
        //
        // arguments - currency,dictionaries
        // * dictionaries must contain currenciesListDictionary block
        // or isNational will be computed
        // due to default currency settings
        //
        // returns boolean
        isNationalCurrency: function(data) {
            var currency = data && data.currency ? data.currency : data;
            var dictionaries = data && data.dictionaries ? data.dictionaries : undefined;

            // console.log('!DEBUG! helper isNationalCurrency currency:', currency);
            // console.log('!DEBUG! helper isNationalCurrency dictionaries:', dictionaries);
            if (Array.isArray(currency)) currency = currency[1];
            if (dictionaries && dictionaries.currenciesListDictionary && dictionaries.currenciesListDictionary.currencyRecord) {
                var filteredCurrency = dictionaries.currenciesListDictionary.currencyRecord.filter(function(item) {
                    return (item.currency === currency);
                });
                // console.log('!DEBUG! helper isNationalCurrency RESULT:', (filteredCurrency && filteredCurrency.length>0 ? filteredCurrency[0].isNational: false));
                return filteredCurrency && filteredCurrency.length > 0 ? filteredCurrency[0].isNational : false;
            } else {
                // console.log('!DEBUG! helper isNationalCurrency RESULT:', currency === settings.currency);
                return currency === settings.currency;
            };
        },

        getNationalCurrency: function(data) {
            var dictionaries = data && data.dictionaries ? data.dictionaries : undefined;
            if (dictionaries && dictionaries.currenciesListDictionary && dictionaries.currenciesListDictionary.currencyRecord) {
                var filteredCurrency = dictionaries.currenciesListDictionary.currencyRecord.filter(function(currency){
                    return (currency.isNational)
                });
                if (filteredCurrency && filteredCurrency.length > 0) {
                    return filteredCurrency[0].currency || '';
                };
            } else {
                return '';
            };
        },

        // Checks whether the date is Business day or not
        //
        // arguments - date,currency,dictionaries
        //
        // * dictionaries must contain calendar block
        //   or isBusinessDay will be returned as true
        // * currency can be boolean if it is already known that it is National or not
        //   otherwise it will be computed, using isNational func
        //
        // returns boolean
        isBusinessDay: function(data) {
            var date = data.date;
            var currency = data.currency;
            var dictionaries = data.dictionaries;

            // console.log('!DEBUG! helper isBusinessDay date:', date);
            // console.log('!DEBUG! helper isBusinessDay currency:', currency);
            // console.log('!DEBUG! helper isBusinessDay isNationalCurrency:', helper.isNationalCurrency({currency: currency, dictionaries: dictionaries}));
            // console.log('!DEBUG! helper isBusinessDay dictionaries:', dictionaries);
            if (currency === true || !currency ) {
                currency = !!currency;
            } else {
                currency = helper.isNationalCurrency({
                    currency: currency,
                    dictionaries: dictionaries
                });
            };

            var type = currency ? 'localCurrencyAllowed' : 'foreignCurrencyAllowed';
            var checkDate = moment(date).startOf('day');

            var isBusinessDay = true;
            var COT;
            if (dictionaries && dictionaries.calendar) {
                COT = dictionaries.calendar.calendarRecord;
            }

            for (var i in COT) {
                if (COT && COT[i] && COT[i].calendarDate) {
                    var COTCalendarDate = COT[i].calendarDate;
                    var calendarDate = moment(COTCalendarDate).startOf('day');
                    if (calendarDate.diff(checkDate) === 0) {
                        isBusinessDay = COT[i][type];
                        break;
                    };
                }
            };

            // console.log('!DEBUG! helper isBusinessDay RESULT:', isBusinessDay);
            return isBusinessDay;
        },

        // Checks whether the date is the time after CutOff
        //
        // arguments - date,currency,dictionaries, urgent, transferType
        // * if date > today isCOTPast will be returned as false
        // * dictionaries must contain cutoffTimes block
        //   or isCOTPast will be returned as false
        // * currency can be boolean if it is already known that it is National or not
        //   otherwise it will be computed, using isNational func
        // * urgent - boolean
        //   if true  - used for priority-checker
        //   if false - used for execution-date
        // * transferType - foreign / ownAcc / domestic
        //
        // returns boolean
        isCOTPast: function(data) {
            var date = data.date;
            var dictionaries = data.dictionaries;
            var transferType = data.transferType;
            var urgent = data.urgent;
            var currency = data.currency;
            var isIntraBankTransfer = data.isIntraBankTransfer;
            // console.log('!DEBUG! helper isCOTPast date:', date);
            // console.log('!DEBUG! helper isCOTPast currency:', currency);
            // console.log('!DEBUG! helper isCOTPast urgent:', urgent);
            // console.log('!DEBUG! helper isCOTPast transferType:', transferType);
            // console.log('!DEBUG! helper isCOTPast dictionaries:', dictionaries);
            var dd = moment(date).startOf('day');
            var today = moment().startOf('day');

            if (dd.diff(today) === 0) {
                var now = moment().format('HH:mm');

                if (currency === true || !currency ) {
                    currency = !!currency;
                } else {
                    currency = helper.isNationalCurrency({currency: currency, dictionaries: dictionaries});
                };

                if (!urgent) {
                    switch (transferType){
                        case 'foreign':
                            var typeCOT = 'foreignNonUrgentString';
                        break;
                        case 'ownAcc':
                            var typeCOT = currency ? 'ownAccNationalString' : 'ownAccForeignString';
                        break;
                        case 'domestic':
                            if (isIntraBankTransfer) {
                                var typeCOT = currency ? 'intrabankNationalString' : 'intrabankForeignString';
                            } else {
                                var typeCOT = currency ? 'domesticNCCYNonUrgentString' : 'domesticFCCYNonUrgentString';
                            }
                            // console.log('======', typeCOT)
                        break;
                    };
                } else {
                    var typeCOT = transferType === 'foreign' ? 'foreignUrgentString' : currency ? 'domesticNCCYUrgentString' : 'domesticFCCYUrgentString';
                };
                if (dictionaries && dictionaries.cutoffTimes) {
                    var COT = dictionaries.cutoffTimes[typeCOT];
                    // console.log('!DEBUG! helper For',(urgent ? 'urgent': 'non-urgent'),transferType, 'transfer, COT is:', typeCOT, ': ',COT, 'and it is', (now > COT ? 'PAST' : 'NOT PAST'));
                    return (now > COT);
                };
                // console.log('!DEBUG! helper For', (urgent ? 'urgent': 'non-urgent'),transferType, 'transfer, COT is:', typeCOT, ': UNKNOW due to undefined dicitonaries, so COT is NOT PAST');
                return false;
            } else {
                // console.log('!DEBUG! helper For', (urgent ? 'urgent': 'non-urgent '),transferType, 'transfer, COT is NOT PAST, because execution date is NOT today');
                return false;
            };
        },

        // Returns next business day after the one, presented in date argument
        //
        // arguments - date,currency,dictionaries
        // * dictionaries must contain calendar block
        //   or date argument will be returned as next business day
        // * currency can be boolean if it is already known that it is National or not
        //   otherwise it will be computed, using isNational func
        //
        // returns Date object
        getNextBusinessDay: function(data) {
            var date = data.date;
            var dictionaries = data.dictionaries;
            var currency = data.currency;
            // console.log('!DEBUG! helper getNextBusinessDay date:', date);
            // console.log('!DEBUG! helper getNextBusinessDay currency:', currency);
            // console.log('!DEBUG! helper getNextBusinessDay dictionaries:', dictionaries);

            var dd=moment(date).startOf('day');
            var today=moment().startOf('day');

            if (currency === true || !currency ) {
                currency = !!currency;
            } else {
                currency = helper.isNationalCurrency({currency: currency, dictionaries: dictionaries});
            };

            var type = currency ? 'localCurrencyAllowed' : 'foreignCurrencyAllowed';

            if (dictionaries && dictionaries.calendar) {
                var calendar = dictionaries.calendar.calendarRecord;
                for (var i in calendar) {
                    var dateCalendar=moment(calendar[i].calendarDate);
                    if (dateCalendar.startOf('day').diff(dd)>0 && dateCalendar.startOf('day').diff(today)>0 && calendar[i][type]) {
                        // console.log('!DEBUG! helper getNextBusinessDay RESULT:', new Date(calendar[i].calendarDate));
                        return new Date(calendar[i].calendarDate);
                        break;
                    };
                };
            }
            else {
                // console.log('!DEBUG! helper getNextBusinessDay RESULT:', new Date(date));
                return new Date(date);
            };
        },

        // Returns last available business day due to futureDateXXXTransferMaxDays parameters
        //
        // arguments - date,currency,dictionaries, transferType
        // * dictionaries must contain calendar block
        //   or lastDay var will be returned as last business day
        // * currency can be boolean if it is already known that it is National or not
        //   otherwise it will be computed, using isNational func
        // * transferType - foreign / ownAcc / domestic
        //
        // returns Date object
        getLastBusinessDay: function(data) {
            var date = data.date;
            var dictionaries = data.dictionaries;
            var transferType = data.transferType;
            var currency = data.currency;

            // console.log('!DEBUG! getLastBusinessDay date:', date);
            // console.log('!DEBUG! getLastBusinessDay transferType:', transferType);
            // console.log('!DEBUG! getLastBusinessDay dictionaries:', dictionaries);
            // console.log('!DEBUG! getLastBusinessDay currency:', currency);

            var maxDays = dictionaries['futureDate'+ transferType.charAt(0).toUpperCase() + transferType.slice(1) +'TransferMaxDays'] ?
                dictionaries['futureDate'+ transferType.charAt(0).toUpperCase() + transferType.slice(1) +'TransferMaxDays'] :
                settings['futureDate'+ transferType.charAt(0).toUpperCase() + transferType.slice(1) +'TransferMaxDays'];
            var lastDay = moment((new Date()).getTime()+86400000*maxDays).startOf('day');

            if (currency === true || !currency ) {
                currency = !!currency;
            } else {
                currency = helper.isNationalCurrency({currency: currency, dictionaries: dictionaries});
            };

            var type = currency ? 'localCurrencyAllowed' : 'foreignCurrencyAllowed';

            if (dictionaries && dictionaries.calendar) {
                var calendar = dictionaries.calendar.calendarRecord.filter(function(item) {
                    return (
                        moment(item.calendarDate).startOf('day').diff(lastDay)>=-7 &&
                        moment(item.calendarDate).startOf('day').diff(lastDay)<=0
                    );
                });

                for (var i = calendar.length-1; i >= 0; i--) {
                    if  (helper.isBusinessDay({date: calendar[i].calendarDate, currency: currency , dictionaries: dictionaries })) {
                        // console.log('!DEBUG! getLastBusinessDay RESULT:', new Date(calendar[i].calendarDate));
                        return new Date(calendar[i].calendarDate);
                        break;
                    }
                };
            }
            else {
                // console.log('!DEBUG! getLastBusinessDay RESULT:', new Date(lastDay));
                return new Date(lastDay);
            };
        },

        // Returns true/false whether checked date if less than
        // maximum allowed future date for transfers due to BH (MCP) settings
        //
        // arguments - date,dictionaries,transferType
        // * dictionaries must contain parameter futureDateXXXTransferMaxDays
        // if dictionaries is undefined - uses default, described in FSD values for this arguments
        // * transferType - foreign / ownAcc / domestic
        //
        // returns Boolean
        isBeforeFutureMaxDays: function(data) {
            var date = data.date;
            var dictionaries = data.dictionaries;
            var transferType = data.transferType;

            var maxDays = dictionaries && dictionaries['futureDate'+ transferType.charAt(0).toUpperCase() + transferType.slice(1) +'TransferMaxDays'] ?
                dictionaries['futureDate'+ transferType.charAt(0).toUpperCase() + transferType.slice(1) +'TransferMaxDays'] :
                    settings['futureDate'+ transferType.charAt(0).toUpperCase() + transferType.slice(1) +'TransferMaxDays'];
            var lastDay = (new Date()).getTime()+86400000*maxDays;

            // console.log('!DEBUG! isBeforeFutureMaxDays date', date );
            // console.log('!DEBUG! isBeforeFutureMaxDays dictionaries', dictionaries );
            // console.log('!DEBUG! isBeforeFutureMaxDays transferType', transferType );
            // console.log('!DEBUG! isBeforeFutureMaxDays maxDays', maxDays );
            // console.log('!DEBUG! isBeforeFutureMaxDays result', (date.getTime() < lastDay) );

            return date.getTime() < lastDay;
        },

        // Returns formatted value of account number, by fetched model
        formatAccountNumber: function(account, daily) {
            if (settings.bank === 'SBCZ' && !daily) {
                return  (account.accountPrefix || '') + ' - ' + (account.accountNumberLocal || '') + ' / ' + (account.accountBankCode || '');
                // code was removed by MCPI-971
                /*
                if ( account.currency && !helper.isNationalCurrency(account.currency)) {
                    return account.accountNumberIBAN || '';
                } else {
                    return  (account.accountPrefix || '') + ' - ' + (account.accountNumberLocal || '') + ' / ' + (account.accountBankCode || '');
                };
                */
            } else {
                return account.accountNumberIBAN || '';
            }
        },

        makeElementVisibleOnViewport: function (element) {
            var top = element.getBoundingClientRect().top;

            if (top < 0) {
                var appElement = document.getElementById("app");

                appElement.scrollTop = appElement.scrollTop + top;
            }
        },

        getErrorMessage: function (code, message) {
            if (settings && settings.errorCodes && settings.errorCodes[code]) {
                return settings.errorCodes[code];
            } else {
                return (message ? message : 'ERRCODE_1000_UNKNOWN_ERROR');
            }; 
        },

        response2error: function (response) {
            if (response instanceof Error) { //если упало где-то в коде, промис возвращает не ответ сервера, а ошибку 
                return response.message;
            } else {
                return (response && response.resultCode != 0 
                  ? "ERROR " + response.resultCode + (response.resultMessage ? "  " + response.resultMessage : "") 
                  : undefined);
            }
        },

    isIBAN: function(source) {
        if (typeof source != "string") return false;

        //return source.length == 3;
        if (source.length < 5) return false;

        var nonEscapedNumber = source.substring(4) + source.substring(0, 4);
        var number = nonEscapedNumber.replace(/./g, escape);

        if (isNaN(number)) return false;

        return bigInteger(number, 10).mod(97).toString() == "1";

        function escape(char) {
            if (/\d/.test(char)) return char;

            var key = char.toUpperCase(); // ???

            return IBANCharsMap.hasOwnProperty(key) ? IBANCharsMap[key] : "g";
        }
    },

    checkIBANCountryCode: function (IBAN) {
        if (typeof IBAN != "string") return false;

        var ibanCountryCode = IBAN.substring(0, 2).toUpperCase();

        return IBANCountryCodeList.indexOf(ibanCountryCode) != -1;
    },


        createDate: function (initial) {
            if (!initial) return null;

            var date = new Date(initial);

            return isNaN(date.getTime()) ? null : date;
        },

        datesIsEquil:  function (dateA, dateB) {
            return helper.dateFormat(dateA) == helper.dateFormat(dateB);
        },

    parseInt: function(value, min_value, default_value) {
        var nn = parseInt(value);
        return isNaN(nn) ? default_value : (nn <= min_value ? default_value : nn)
    },

    isCroatianIBAN: function(source) {
        if (typeof source != "string") throw new TypeError;

        return /^HR\d{19}$/.test(source);
    },

    isGermanIBAN: function(source) {
        if (typeof source != "string") throw new TypeError;

        return /^HR\d{20}$/.test(source);
    },

    ABCDEFGHIJAlgorithm: function(string) {
        if (typeof string != "string") throw new TypeError;

        var weights = [6, 3, 7, 9, 10, 5, 8, 4, 2, 1];
        var chars = this.leftPad(string, weights.length, "0").split("");
        
        return chars.map(weightsAssign).reduce(toSum) % 11 === 0;
        

        function weightsAssign(char, index) {
            return Number(char) * weights[index];
        }

        function toSum(sum, number) {
            return sum + number;
        }
    },

    leftPad: function (string, length, pad) {
        if (typeof string != "string") throw new TypeError;

        var limit = length - string.length;
        var step = pad.length;

        for (var i = 0; i < limit; i += step) {
            string = pad + string;
        }

        return string;
    },

        startsWith: function(str, starts){
            if (starts === '') return true;
            if (str == null || starts == null) return false;
            str = String(str); starts = String(starts);
            return str.length >= starts.length && str.slice(0, starts.length) === starts;
        },

        sortObjFieldsAlphabetically : function (data) {
            var sorted = {};
            Object.keys(data).sort(function(a, b){
                return a < b ? -1 : 1
            }).forEach(function(key){
                sorted[key] = data[key];
                if (typeof sorted[key] == 'object' && !(sorted[key] instanceof Array))
                    sorted[key] = helper.sortObjFieldsAlphabetically(sorted[key]);
            });
            return sorted;
        },
        // функция сортировки массива JSON объектов
        // ArrayOfJSONs.sort(sortJSON('keyName', isDESCBool, dataPrepareFunc));
        // Sort by price high to low
        // homes.sort(sortJSON('price', true, parseInt));
        // Sort by city, case-insensitive, A-Z
        // homes.sort(sortJSON('city', false, function(a){return a.toUpperCase()}));

        sortJSON : function(field, reverse, primer){
            var key = primer ? 
            function(x) {return primer(x[field])} : 
            function(x) {return x[field]};

            reverse = !reverse ? 1 : -1;
            return function (a, b) {
                return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
            };
        },
        groupBy : function(array , f) {
            var groups = {};
            array.forEach( function(o) {
                var group = JSON.stringify(f(o));
                groups[group] = groups[group] || [];
                groups[group].push(o);  
            });
            return Object.keys(groups).map(function(group) {
                return groups[group]; 
            });
        }
    };
//}());

for (var i in helper) if (typeof helper[i] === 'function') helper[i] = helper[i].bind(helper);

if (typeof navigator !== 'undefined' && navigator.userAgent) {
    helper.downloadFile.isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') !== -1;
    helper.downloadFile.isSafari = navigator.userAgent.toLowerCase().indexOf('safari') !== -1;
}

module.exports = helper;
