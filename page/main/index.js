import './styles.css';

import React       from 'react';
import { connect } from 'react-redux'
import {togglePopupAsk, popupAskChanged, popupAskSetError, popupAskSubmit } from '../../actions/pageMain';;

import Header from '../../components/header';
import Menu from '../../components/menu';
import PhotoBlock from '../../components/photo-block';
import InfoBlock from '../../components/info-block';
import Popup from '../../components/popup';
import Textarea from 'react-textarea-autosize';



class PageMain extends React.Component {
    constructor(props) {
        super(props);
    }

    togglePopupAsk() {
        this.props.dispatch(togglePopupAsk());
    }
    popupAskChanged(type, e){
        let data={};
        data[type]=e.target.value;
        this.props.dispatch(
            popupAskChanged(data)
        );
    }
    popupAskSubmit(){
        const {
            popupAskName,
            popupAskEmail,
        } = this.props.state;
        if (!popupAskName || !popupAskEmail) {
            if (!popupAskName) this.props.dispatch(popupAskSetError('popupAskName'));
            if (!popupAskEmail) this.props.dispatch(popupAskSetError('popupAskEmail'));
        }
        this.props.dispatch(popupAskSubmit());
    }
    renderPopupAsk(){
        const {
            errors=[],
            popupAskName,
            popupAskQuestion,
            popupAskEmail,
        } = this.props.state,
        popupAskChanged = this.popupAskChanged,
        popupAskSubmit = this.popupAskSubmit.bind(this),
        togglePopupAsk = this.togglePopupAsk.bind(this);
        return (
            <Popup title="Задать вопрос" width="50%" onClose={togglePopupAsk}> 
                <div className="popup-line">
                    <div className="popup-input-label">Ваше имя*</div>
                    <input 
                        className={"popup-input"+(~errors.indexOf('popupAskName')?' popup-input-error': '') }
                        ref="popupName" 
                        onChange={popupAskChanged.bind(this,'popupAskName')} 
                        value={popupAskName}
                    />
                </div>
                <div className="popup-line">
                    <div className="popup-input-label">E-mail*</div>
                    <input 
                        className={"popup-input"+(~errors.indexOf('popupAskEmail')?' popup-input-error': '') } 
                        ref="popupEmail" 
                        onChange={popupAskChanged.bind(this,'popupAskEmail')} 
                        value={popupAskEmail}
                    />
                </div>
                <div className="popup-line">
                    <div className="popup-input-label">Вопрос</div>
                    <Textarea 
                        className="popup-input" 
                        ref="popupQuestion" 
                        onChange={popupAskChanged.bind(this,'popupAskQuestion')} 
                        value={popupAskQuestion}
                    />
                </div>
                <div className="popup-line right">
                    <button className="popup-button" onClick={popupAskSubmit}>отправить</button>
                </div>
                <div className="popup-info">Поля, отмеченные знаком *, обязательны для заполнения</div>
            </Popup>
        )
    }

    render() {
        const {
            popupAskVisibile,
        } = this.props.state,
        togglePopupAsk = this.togglePopupAsk.bind(this);
        return (
            <div className="page-main">
                <Header clickAsk={togglePopupAsk} /> 
                <Menu /> 
                <PhotoBlock />
                <InfoBlock 
                    title="наши специалисты"
                    about="Все специалисты нашего Института проходят тщательный отбор среди опытных практикующих психологов психотерапевтов, успешно выдержавших экзамены на право применения метода «Интегральное нейропрограммирование». Специалисты работают под супервизией и научным руководством Сергея Викторовича Ковалева."
                >
                    <div className="circle-block">
                        <div className="info-block-circle lama"/>
                        <div className="info-block-circle-title">ДАЛАЙ ДАЛАЕВИЧ ЛАМА</div>
                        <div className="info-block-circle-about">Живет вечно, лечит рак, дарит бессмертие</div>
                    </div>
                    <div className="circle-block">
                        <div className="info-block-circle kurpatov"/>
                        <div className="info-block-circle-title">АНДРЕЙ КУРПАТОВ</div>
                        <div className="info-block-circle-about">Способен справиться с любой формой шизофрении и ментального расстройства</div>
                    </div>
                    <div className="circle-block">
                        <div className="info-block-circle konfuciy"/>
                        <div className="info-block-circle-title">КОНФУЦИЙ</div>
                        <div className="info-block-circle-about">Знает дохера мудрого. Не лечит, просто держит палец кверху.</div>
                    </div>
                    <div className="circle-block">
                        <div className="info-block-circle sobut"/>
                        <div className="info-block-circle-title">СОБУТЫЛЬНИК</div>
                        <div className="info-block-circle-about">Самый эфективный терапевт столетия</div>
                    </div>
                    <div className="circle-block">
                        <div className="info-block-circle frejd"/>
                        <div className="info-block-circle-title">ФРЕЙД</div>
                        <div className="info-block-circle-about">Если есть проблемы, о которых не говорят вслух...</div>
                    </div>
                </InfoBlock>
                <InfoBlock 
                    title="отзывы о нас"
                    about="Ниже представлены реальные отзывы клиентов, которым мы помогли. Если Вы работали с нами и хотите оставить отзыв - сделайте это! Это поможет другим в принятии решения начать жить по-новому!"
                >
                    <div className="review-block">
                        <div className="review-text">
                            Praesent in mauris eu tortor porttitor accumsan. Mauris suscipit, ligula sit amet pharetra semper, nibh ante cursus purus, vel sagittis velit mauris vel metus. Aenean fermentum risus id tortor. Integer imperdiet lectus quis justo. Integer tempor. Vivamus ac urna vel leo pretium faucibus. Mauris elementum mauris vitae tortor. In 
                        </div>
                        <div className="review-name">
                            Петр
                        </div>
                        <div className="review-text">
                            Etiam liula pede, sagittis quis, interdum ultricies, scelerisque eu, urna. Nullam at arcu a est sollicitudin euismod. Praesent dapibus. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Nam sed tellus id magna elementum tincidunt. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi gravida libero nec velit. Morbi scelerisque luctus velit. Etiam dui sem, fermentum vitae, sagittis id, malesuada in, quam. Proin mattis lacinia justo. Vestibulum facilisis auctor urna. Aliquam in lorem sit amet leo accumsan lacinia. Integer rutrum, orci vestibulum ullamcorper ultricies, lacusg quam ultricies odio, vitae placerat pede sem sit amet enim. Phasellus et lorem id felis nonummy placerat. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci. Aenean vel massa quis mauris vehicula lacinia.
                        </div>
                        <div className="review-name">
                            Ирина
                        </div>
                        <div className="review-text">
                            Mauris suscipit, ligula sit amet pharetra semper, nibh ante cursus purus, vel sagittis velit mauris vel metus. 
                        </div>
                        <div className="review-name">
                            Степан
                        </div>
                    </div>
                </InfoBlock>
                {popupAskVisibile && this.renderPopupAsk()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        state : state.pageMain,
    }
}
export default connect (mapStateToProps) (PageMain);
