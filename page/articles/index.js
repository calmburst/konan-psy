import React       from 'react';
import { connect } from 'react-redux'

import './styles.css';

class PageArticles extends React.Component {
    render() {
        return (
            <div className="page-articles">
                СТАТЬИ
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        state : state.PageArticles
    }
}
export default connect (mapStateToProps) (PageArticles);
