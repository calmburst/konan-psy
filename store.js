import {createStore, compose, applyMiddleware} from 'redux';
import reducer    from './reducers';
import thunk      from 'redux-thunk';
 
let middlewares = [thunk];
let enhancer;

if ((typeof __DEV__ !== 'unefined') || __DEV__ === true) {
	const createLogger = require('redux-logger');
	const logger = createLogger({
		duration: false,
		timestamp: false,
		collapsed: true
	});

    middlewares.push(logger);
    enhancer = compose(
        applyMiddleware(...middlewares)
    );

} else {
    enhancer = compose(
        applyMiddleware(...middlewares)
    );
}

const store = createStore(reducer, enhancer);

if (__DEV__ === true) {
	window.store = store;
}

export default store;
