import {Promise} from 'es6-promise';

// <ActionTypes
export const POPUP_ASK_CHANGED = 'POPUP_ASK_CHANGED';
export const POPUP_ASK_SUBMIT = 'POPUP_ASK_SUBMIT';
export const POPUP_ASK_SET_ERROR = 'POPUP_ASK_SET_ERROR';
export const TOGGLE_POPUP_ASK = 'TOGGLE_POPUP_ASK';
// ActionTypes>

// <ActionCreators
export function popupAskChanged (data) {
    return {
        type: POPUP_ASK_CHANGED,  
        data
    }
}
export function popupAskSetError (field) {
    return {
        type: POPUP_ASK_SET_ERROR,
        field
    }
}
export function popupAskSubmit () {
    return {
        type: POPUP_ASK_SUBMIT
    }
}
export function togglePopupAsk () {
    return {
        type: TOGGLE_POPUP_ASK,  
    }
}
// ActionCreators>