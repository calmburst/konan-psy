import { combineReducers }      from 'redux';
import {reducer as formReducer} from 'redux-form';

import menu               from './menu';
import pageMain           from './pageMain';

export default combineReducers({
    menu,
    pageMain,
});