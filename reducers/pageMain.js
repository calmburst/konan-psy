import _ from '../helper';
// <ActionTypes
import {
	POPUP_ASK_CHANGED,
	POPUP_ASK_SET_ERROR,
	POPUP_ASK_SUBMIT,
    TOGGLE_POPUP_ASK,
	} from '../actions/pageMain';
// ActionTypes>


export default function (state = {}, action) {
    switch (action.type) {
        case POPUP_ASK_CHANGED:
	        var newState = Object.assign({},state, action.data);
	        if (!newState.errors) newState.errors=[];
	        if ( ~newState.errors.indexOf(Object.keys(action.data)[0]))  newState.errors.splice(newState.errors.indexOf(Object.keys(action.data)[0]),1);
	        return newState; 

	    case POPUP_ASK_SET_ERROR:
	        var newState = Object.assign({},state);
	        if (!newState.errors) newState.errors=[];
	        newState.errors.push(action.field);
	        return newState;

	    case POPUP_ASK_SUBMIT:
	        var newState = Object.assign({},state);
	        if (!newState.errors || !newState.errors.length) newState.popupAskVisibile = false;
	        return newState;

	    case TOGGLE_POPUP_ASK:
	        var newState = Object.assign({},state);
	        newState.popupAskVisibile = !newState.popupAskVisibile;
	        return newState;

        default:
            return state;
    }
} 