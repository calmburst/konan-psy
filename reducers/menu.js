import _ from '../helper';
// <ActionTypes
import {
    GET_MENU,
	} from '../actions/menu';
// ActionTypes>


export default function (state = {}, action) {
    switch (action.type) {
        case GET_MENU:
            return [
                {
                    name: 'главная',
                    href:'/#',
                },{
                    name: 'мероприятия',
                    href:'#/events',
                    children:[ 
                        {
                            name: 'расписание мероприятий',
                            href:'#/events',
                        },{
                            name: 'отчеты о мероприятиях',
                            href:'#/events-reports',
                        },{
                            name: 'отзывы о мероприятиях',
                            href:'#/events-reviews',
                        },
                    ]
                },{
                    name: 'индивидуальный прием',
                    href:'#/healing',
                    children:[ 
                        {
                            name: 'как происходит психотерапия',
                            href:'#/healing',
                        },{
                            name: 'стоимость услуг',
                            href:'#/healing-prices',
                        },{
                            name: 'запись на консультацию',
                            href:'#/healing-contact',
                        },
                    ]
                },{
                    name: 'библиотека',
                    href:'#/library',
                    children:[ 
                        {
                            name: 'статьи',
                            href:'#/library-articles',
                        },{
                            name: 'видео',
                            href:'#/library-video',
                        },{
                            name: 'книги',
                            href:'#/library',
                        },
                    ]
                },{
                    name: 'о нас',
                    href:'#/about',
                    children:[ 
                        {
                            name: 'наши специалисты',
                            href:'#/about',
                        },{
                            name: 'новости',
                            href:'#/about-news',
                        },
                    ]
                },{
                    name: 'контакты',
                    href:'#/contacts',
                },
            ];

        default:
            return state;
    }
}